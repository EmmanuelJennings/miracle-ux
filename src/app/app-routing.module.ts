import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccordionComponent } from './accordion/accordion.component';


const appRoutes: Routes = [
  {
    path: '',
    component: AccordionComponent
  },
  {
    path: 'accordion',
    component: AccordionComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

